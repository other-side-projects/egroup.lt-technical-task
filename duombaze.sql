-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 17, 2013 at 05:49 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `egroup_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `grupes`
--

CREATE TABLE IF NOT EXISTS `grupes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Vardas` varchar(45) DEFAULT NULL,
  `moduliu_teises` varchar(45) NOT NULL DEFAULT '0',
  `moduliu_daliu_teises` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `Vardas_UNIQUE` (`Vardas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `grupes`
--

INSERT INTO `grupes` (`id`, `Vardas`, `moduliu_teises`, `moduliu_daliu_teises`) VALUES
(1, 'Pirma', '0', '0'),
(2, 'Antra', '0', '0'),
(3, 'trecia', '0', '0'),
(4, 'ketvirta', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `moduliai`
--

CREATE TABLE IF NOT EXISTS `moduliai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vardas` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vardas_UNIQUE` (`vardas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `moduliai`
--

INSERT INTO `moduliai` (`id`, `vardas`) VALUES
(2, 'antras'),
(4, 'ketvirtas'),
(1, 'Pirmas'),
(3, 'trecias');

-- --------------------------------------------------------

--
-- Table structure for table `moduliu_dalys`
--

CREATE TABLE IF NOT EXISTS `moduliu_dalys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pavadinimas` varchar(45) DEFAULT NULL,
  `moduliai_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pavadinimas_UNIQUE` (`pavadinimas`),
  KEY `fk_moduliu_dalys_moduliai1_idx` (`moduliai_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `moduliu_dalys`
--

INSERT INTO `moduliu_dalys` (`id`, `pavadinimas`, `moduliai_id`) VALUES
(1, 'pirma pirmo', 1),
(2, 'Pirma antro', 2),
(3, 'antra pirmo', 1),
(4, 'pirma trecio', 3),
(5, 'pirma ketvirto', 4),
(6, 'antra ketvirto', 4);

-- --------------------------------------------------------

--
-- Table structure for table `vartotojai`
--

CREATE TABLE IF NOT EXISTS `vartotojai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vardas` varchar(45) DEFAULT NULL,
  `grupes_id` int(11) NOT NULL,
  `moduliu_teises` varchar(45) NOT NULL DEFAULT '0',
  `moduliu_daliu_teises` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vardas_UNIQUE` (`vardas`),
  KEY `fk_vartotojai_grupes_idx` (`grupes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `vartotojai`
--

INSERT INTO `vartotojai` (`id`, `vardas`, `grupes_id`, `moduliu_teises`, `moduliu_daliu_teises`) VALUES
(1, 'Pirmas', 1, '0', '0'),
(2, 'antras', 4, '0', '0'),
(3, 'trecias', 2, '20', '0'),
(4, 'ketvirtas', 3, '0', '0'),
(5, 'penktas', 1, '0', '0'),
(6, 'sestas', 1, '0', '0'),
(7, 'septingas', 4, '0', '0'),
(8, 'astuntas', 3, '0', '0'),
(9, 'devintas', 2, '0', '0'),
(10, 'desimtas', 3, '0', '0'),
(11, 'vienuoliktas', 1, '0', '0'),
(12, 'dvyliktas', 4, '0', '0'),
(13, 'tryliktas', 2, '0', '0'),
(14, 'keturioliktas', 4, '0', '0'),
(15, 'penkioliktas', 2, '0', '0');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `moduliu_dalys`
--
ALTER TABLE `moduliu_dalys`
  ADD CONSTRAINT `fk_moduliu_dalys_moduliai1` FOREIGN KEY (`moduliai_id`) REFERENCES `moduliai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `vartotojai`
--
ALTER TABLE `vartotojai`
  ADD CONSTRAINT `fk_vartotojai_grupes` FOREIGN KEY (`grupes_id`) REFERENCES `grupes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

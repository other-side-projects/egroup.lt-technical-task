<?php
require_once 'permissions.class.php';
require_once 'permissions.extended.class.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
		<?php
		$user = 'trecias';
		$modulio_dalis = 'Pirma ketvirto';

		$perms = new permissions_extended();

		echo "Vartotojas: '$user', modulio dalis: '$modulio_dalis'<br />";
		//var_dump($perms->checkUser($user, $modulio_dalis));
		//Check what level is needed for a permission.
		$modLevel = $perms->permNeededExt(array('Pirmas'), 'modulis');
		echo '<br />Norint pasiekt modulį, kurį nurodėte reikia teisės:' . $modLevel;
		$modLevel = $perms->permNeededExt(array('Pirma antro'), 'dalis');
		echo '<br />Norint pasiekt modulio dalį, kurią nurodėte reikia teisės:' . $modLevel;
		?>
    </body>
</html>

<?php

class Permission {

	protected $users;
	protected $perms;

	function __construct($users, $perms) {
		$this->users = $users;
		$this->perms = $perms;
	}

	public function hasAccess($user, $perm) {
		if (isset($user) && array_key_exists($perm, $this->perms) && array_key_exists($user, $this->users)) {
			if ((int) $this->users[$user] & (int) $this->perms[$perm]) {
				return true;
			}
		}
	}

	public function permNeeded($perms) {
		if (is_array($perms)) {
			$total = 0;
			foreach ($perms as $perm) {
				if (array_key_exists($perm, $this->perms)) {
					$total += $this->perms[$perm];
				}
			}
			return $total;
		}
	}

	public function can($user) {
		if (isset($user) && array_key_exists($user, $this->users)) {
			$permissions = Array();
			foreach ($this->perms as $k => $v) {
				if ($this->hasAccess($user, $k)) {
					$permissions[] = $k;
				}
			}
			return $permissions;
		}
	}

}

?> 
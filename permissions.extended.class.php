<?php

/**
 * Description of permissions
 *
 * @author sanis
 */
class permissions_extended extends Permission {

	private $db;

	public function __construct() {
		try {
			$this->db = new PDO('mysql:host=localhost;dbname=egroup_new;charset=utf8', 'root', '');
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}

	public function checkUser($user, $modulepart) {

		$stmt = $this->db->prepare("SELECT 
				vartotojai.id AS vartotojo_id,
				grupes.id as grupes_id,
				grupes.moduliu_teises as grupes_moduliu_teises,
				grupes.moduliu_daliu_teises as grupes_moduliu_daliu_teises,
				vartotojai.moduliu_teises as vartotojo_moduliu_teises,
				vartotojai.moduliu_daliu_teises as vartotojo_moduliu_daliu_teises							
				FROM vartotojai
				INNER JOIN grupes
				ON vartotojai.grupes_id=grupes.id
				WHERE vartotojai.vardas=?");
		$stmt->bindValue(1, $user, PDO::PARAM_STR);
		$stmt->execute();
		$user_info = $stmt->fetch(PDO::FETCH_ASSOC);

		$stmt = $this->db->prepare("SELECT 
				moduliai.id as modulio_id,
				moduliai.vardas as modulio_vardas,
				moduliu_dalys.id modulio_dalies_id,
				moduliu_dalys.pavadinimas modulio_dalies_vardas
				FROM moduliai
				INNER JOIN moduliu_dalys
				ON moduliu_dalys.moduliai_id=moduliai.id
				WHERE moduliu_dalys.pavadinimas=?");
		$stmt->bindValue(1, $modulepart, PDO::PARAM_STR);
		$stmt->execute();
		$modulio_info = $stmt->fetch(PDO::FETCH_ASSOC);

		$stmt = $this->db->query('SELECT * FROM moduliai ORDER BY id ASC');
		$moduliai = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$stmt = $this->db->query('SELECT * FROM moduliu_dalys ORDER BY id ASC');
		$moduliu_dalys = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$moduliu_teises = $this->makeTeises($moduliai);
		$modulio_daliu_teises = $this->makeTeises($moduliu_dalys);

		$esamos_teises_moduliu = array(
			'grupes_moduliu_teises' => $user_info['grupes_moduliu_teises'],
			'vartotojo_moduliu_teises' => $user_info['vartotojo_moduliu_teises']
		);

		$esamos_teises_moduliu_daliu = array(
			'grupes_moduliu_daliu_teises' => $user_info['grupes_moduliu_daliu_teises'],
			'vartotojo_moduliu_daliu_teises' => $user_info['vartotojo_moduliu_daliu_teises']
		);

		//TODO: Optimizuoti kodą žemiau:

		$this->setVars($esamos_teises_moduliu, $moduliu_teises);

		$turi_modulio_priejima_grupe = $this->hasAccess('grupes_moduliu_teises', $modulio_info['modulio_vardas']);
		$turi_modulio_priejima_vartotojas = $this->hasAccess('vartotojo_moduliu_teises', $modulio_info['modulio_vardas']);

		$this->setVars($esamos_teises_moduliu_daliu, $modulio_daliu_teises);

		$turi_modulio_dalies_priejima_grupe = $this->hasAccess('grupes_moduliu_daliu_teises', $modulio_info['modulio_dalies_vardas']);
		$turi_modulio_dalies_priejima_vartotojas = $this->hasAccess('vartotojo_moduliu_daliu_teises', $modulio_info['modulio_dalies_vardas']);

		if ($turi_modulio_dalies_priejima_vartotojas) {
			return true;
		} else if ($turi_modulio_priejima_vartotojas) {
			return true;
		} else if ($turi_modulio_dalies_priejima_grupe) {
			return true;
		} else if ($turi_modulio_priejima_grupe) {
			return true;
		} else {
			return false;
		}
		// OPTIMIZUOTI IKI ČIA
	}
	
	private function makeTeises($is_ko) {
		foreach ($is_ko as $ka) {
			(!isset($ka['vardas'])) ? $key='pavadinimas' : $key='vardas';
			$return[$ka[$key]] = pow(2, $ka['id']);
		}
		return $return;
	}

	private function setVars($user, $perm) {
		$this->users = $user;
		$this->perms = $perm;
	}
	
	public function permNeededExt($perms,$which) {
		if ($which=='modulis') {
			$stmt = $this->db->query('SELECT * FROM moduliai ORDER BY id ASC');
			$moduliai = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$this->perms = $this->makeTeises($moduliai);
		} else if ($which=='dalis') {
			$stmt = $this->db->query('SELECT * FROM moduliu_dalys ORDER BY id ASC');
			$moduliu_dalys = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$this->perms = $this->makeTeises($moduliu_dalys);
		}
		return $this->permNeeded($perms);
		
	}

}

?>
